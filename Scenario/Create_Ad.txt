*** Settings ***
Library           Selenium2Library
Library           AutoItLibrary
Resource          ../Libs/read_data.txt
Resource          ../Libs/Create Ad_Lib.txt
Resource          ../Libs/Create_Used_Ad_lib.txt
Resource          ../Libs/Create_Recon_Lib.txt
Library           BuiltIn
Library           Process

*** Test Cases ***
Select Makes
    ${users}    Read Csv File    D:\\Working\\iCar\\Automationtest\\SourceCode\\Data\\users.csv
    ${length}    Get Length    ${users}
    : FOR    ${i}    IN RANGE    1    ${length}
    \    LoopBrowser_CreateAd

Create Used Ad_MY_CA001
    [Documentation]    This script used for:
    ...    MY_CA002
    ...    MY_CA014
    ...    MY_CA012
    ...    MY_CA019
    ${users}    Read Csv File    D:\\Working\\iCar\\Automationtest\\SourceCode\\Data\\users_used.csv
    ${length}    Get Length    ${users}
    : FOR    ${browser}    IN    @{browsers}
    \    Loop Browser_UsedCar
    [Teardown]

Create Recon Ad
    [Documentation]    This script used for:
    ...    MY_CA003
    ...    MY_CA010
    ...    MY_CA013
    ...    MY_CA018
    ...    MY_CA022
    ${users}    Read Csv File    D:\\Working\\iCar\\Automationtest\\SourceCode\\Data\\users_recon.csv
    ${length}    Get Length    ${users}
    : FOR    ${browser}    IN    @{browsers}
    \    Loop Browser_ReconCar

Create Ad (New Motor) - ID
    ${users}    Read Csv File    ${pathToDataFile}
    ${length}    Get Length    ${users}
    Set Test Variable    ${url}    ${urlID}
    : FOR    ${browser}    IN    @{browsers}
    \    Loop Browser_Create Ad (New Vehicle)    Motor
