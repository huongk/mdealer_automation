*** Settings ***
Test Setup
Resource          ../Libs/common_lib.txt
Resource          ../Libs/login_lib.txt
Library           DatabaseLibrary
Library           AutoItLibrary
Resource          ../Libs/read_data.txt

*** Test Cases ***
MY_LG001
    ${users}    Read Csv File    D:\\Working\\iCar\\Automationtest\\SourceCode\\Data\\users.csv
    ${length}    Get Length    ${users}
    : FOR    ${i}    IN RANGE    1    ${length}
    \    #Loop Browser    Not run with multi browser\
    \    Open Browser    ${urlMY}    ff
    \    TC6084-6088
    #${isCreateAd}    Run Keyword And Return Status    Wait Until Element Is Visible    ${btnCreateAd}    ${timeout}
    #Run Keyword If    ${isCreateAd}== True    docexcel
    Open Excel Current Directory    ExcelRobotTest.xls
    ${col}    Get Row Count    Login
    ${row}    Get Row Count    Login
    : FOR    ${i}    IN RANGE    1    ${row}
    \    ${value}    Get Row Values    Login    ${i}
    \    #Log    ${value[0][${i}]}
    \    Log    ${value[0][1]}
    \    Log    ${i}
    \    Run Keyword If    '${value[0][1]}' == 'MY_LG001'    Exit For Loop
    Put String To Cell    TestSheet4    5    1    @{statusScript}[0]
    Save Excel    ${pathExcelFile}NewIcar1.xls

*** Keywords ***
